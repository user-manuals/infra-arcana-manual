\documentclass[10pt,letterpaper,oneside]{report}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{caption}
\captionsetup{labelformat=empty}

\usepackage{enumitem}
\setlist{topsep=1pt,itemsep=1pt,partopsep=1pt, parsep=1pt}
\setlist[itemize,1]{label=\raisebox{0.5pt}{$\bigstar$}}

\usepackage{graphicx}

\usepackage{background}
\backgroundsetup{
scale=1,
color=black,
opacity=0.4,
angle=0,
contents={%
\includegraphics[width=\paperwidth,height=\paperheight]{mybkg}
}%
}

\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0.0pt}
\fancyhf{}
\fancyfoot[C]{$\longleftarrow$~\thepage~$\longrightarrow$}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\pagestyle{fancy}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=darkgray,
}

\title{\includegraphics[scale=1.5]{infra_arcana_logo1}\\~\\~\\v21.0.1\\manual}
\date{Released April 7, 2022}
\begin{document}
	
\maketitle
	
\newpage
	
\section*{Command list}
\label{sec:commands}

\begin{table}[h!]
	\caption{\textbf{Gameplay commands.}}
	\begin{tabular}{l l}
   		a & \textit{Inventory}, only showing items that can be \textit{consumed} or \textit{activated}\\
   		c & \textit{Close door}, or jam closed door with an iron spike\\
   		C (@) & \textit{Character} information\\
   		d & \textit{Drop} item\\
   		f & \textit{Aim/fire} weapon\\
   		g (,) & \textit{Pick up} item\\
   		h (M) & \textit{Message} history\\
   		i & \textit{Inventory} (wield, equip, remove, consume, or activate items)\\
   		k (w) & \textit{Kick} or strike objects, or \textit{destroy corpses}\\
   		l (e) & Toggle \textit{lantern}\\
   		m & \textit{Minimap}\\
   		n (o) & Make \textit{noise}\\
   		p & \textit{Disarm} trap\\
   		r & \textit{Reload}\\
   		5 (.) & \textit{Wait} one turn (numpad 5 or period)\\
   		 & (This also rearranges pistol magazines if possible,\\
   		 & or feeds on a corpse if playing as the Ghoul character)\\
   		s & \textit{Wait} five turns, or until something happens\\
   		t & \textit{Throw} item, throw lit \textit{explosive}\\
   		u (G) & \textit{Unload} firearm on the ground, or pick up item\\
   		v & \textit{Look, view} descriptions of things on the map\\
   		x & \textit{Cast} spell\\
   		z & \textit{Swap} to readied weapon\\
   		Q & \textit{Quit}\\
   		 & \\
   		Shift & Hold while pressing a move key to move until interrupted\\
   		Tab & Melee attack adjacent monster\\
   	\end{tabular}
\end{table}
	
\begin{table}[h!]
    \caption{\textbf{General menu and window commands.}}
	\begin{tabular}{l l}
        Esc & In-game menu, or cancel/proceed\\
        ?, F1 & This manual\\
        = & Options\\
        +$\backslash$- & Change window width\\
        Ctrl +$\backslash$- & Change window height\\
        Space & Cancel/proceed\\
        Enter & Select something in a menu, confirm\\
	\end{tabular}
\end{table}

\newpage

Move in eight directions with the numpad keys:\newline
\hspace*{8ex} 7 8 9\newline
\hspace*{9ex} $\backslash$$\vert$/\newline
\hspace*{8ex} 4- -6\newline
\hspace*{9ex} /$\vert$$\backslash$\newline
\hspace*{8ex} 1 2 3\newline

Press ``5'' or ``.'' (period) to wait for one turn. This is also used for selecting your current position, for example when kicking (to destroy a corpse you are standing over) or when disarming traps.\newline

As an alternative, for keyboards without a numpad (e.g. some laptop keyboards), the arrow keys can be used for moving. To move diagonally, hold shift or ctrl (auto movement is not supported in this context):\newline
\hspace*{8ex} Shift + left~~~= up left\newline
\hspace*{8ex} Shift + right = up right\newline
\hspace*{8ex} Ctrl~~+ left~~~= down left\newline
\hspace*{8ex} Ctrl~~+ right = down right\newline

``Vi-keys'' are also supported, and can be enabled in the options menu.\linebreak If vi-mode is enabled, the following keys are used for movement instead:\newline
\hspace*{8ex} y k u\newline
\hspace*{9ex} $\backslash$$\vert$/\newline
\hspace*{8ex} h- -l\newline
\hspace*{9ex} /$\vert$$\backslash$\newline
\hspace*{8ex} b j n\newline

The ``k'' and ``j'' keys can be used to scroll up and down in menus, and\linebreak ``l'' selects the currently marked entry (as an alternative to the Enter key).\newline

\section*{Environment interaction}
\label{sec:environment}

Many things in the game can be interacted with by moving into them (i.e. by ``bumping'' them), for example:
\begin{itemize}
	\item Move into monsters to melee attack them
	\item Move into closed doors, chests, cabinets, etc. to attempt to open them
	\item Move into opened chests, cabinets, etc. to loot them
	\item Move into stuck doors to attempt to bash them open
	\item Move into fountains to drink from them
\end{itemize}

\newpage

\section*{The interface}
\label{sec:interface}

On the right hand side of the screen, information is listed about your character and your carried equipment. The following is an example of what these lines can look like:

\begin{table}[h!]
	\caption{\textbf{Gameplay commands.}}
	\begin{tabular}{l r l}
		Level & 1 (60\%) : & Character level, and progress towards next level\\
		~ & ~ & This progress is called experience, or ``XP''\\
		\rule{0ex}{1ex}Depth & 2 : & Current floor (or ``dungeon level'')\\
		\rule{0ex}{2ex}Health & 9/16 : & Current and maximum Hit Points\\
		~ & ~ & See \hyperref[sec:hitpoints]{``Hit Points, Wounds and healing''} for more info\\
		\rule{0ex}{2ex}Spirit & 8/8 : & Current and maximum Spirit Points\\
		~ & ~ & See \hyperref[sec:spirit]{``Spirit and spell casting''} for more info\\
		\rule{0ex}{2ex}Shock & 45\% : & Your level of shock or stress\\
		~ & ~ & See \hyperref[sec:insanity]{``Insanity''} for more info\\
		\rule{0ex}{2ex}Insanity & 12\% : & Your level of insanity\\
		~ & ~ & See \hyperref[sec:insanity]{``Insanity''} for more info\\
		\rule{0ex}{2ex}Wpn & (+1) 8.0 +0\% : & Your currently wielded weapon\\
		~ & ~ & ``(+1)'' means that this weapon does one extra point\\
		~ & ~ & of damage compared to normal weapons of the same kind\\
		~ & ~ & ``8.0'' is your average damage with this weapon\\
		~ & ~ & (including skills affecting the damage)\\
		~ & ~ & ``+0\%'' is this weapon's hit chance modifier (the total\\
		~ & ~ & hit chance is affected by several other factors, such\\
		~ & ~ & as your character's skill and the creature attacked)\\
		\rule{0ex}{2ex}Alt & 8.5 +0\% 4/7 : & Your prepared (alternative weapon)\\
		~ & ~ & The weapon in this example is a firearm, ``4/7'' is the\\
		~ & ~ & weapon's loaded and maximum ammunition\\
		\rule{0ex}{2ex}Lantern & 100 : & Remaining number of turns that your Electric Lantern\\
		~ & ~ & can be used for\\
		\rule{0ex}{2ex}Med. Suppl. & 24 : & Remaining Medical Bag Supplies\\
		~ & ~ & See \hyperref[sec:hitpoints]{``Hit Points, Wounds and healing''} for more info\\
		\rule{0ex}{2ex}Armor & 3 : & Armor Points\\
		~ & ~ & Each point reduces physical damage taken by 1\\
		\rule{0ex}{2ex}Weight & 40\% : & Encumbrance from carrying items\\
		~ & ~ & From 100\% you move at half speed,\\
		~ & ~ & and from 125\% you cannot move at all\\
		\rule{0ex}{2ex}Blind & : & You are currently blind\\
		\rule{0ex}{2ex}Diseased & : & You are currently diseased\\
	\end{tabular}
\end{table}

The properties at the bottom can be temporary properties (e.g. from spells), permanent intrinsic properties (printed in capital letters, e.g. ``CURSED''), or properties granted by wearing or wielding items.

\section*{Inventory handling}
\label{sec:inventory}

The inventory can be accessed in four ways:

(1) Pressing ``i'' opens a full view of your inventory.\newline
Here, you can see all of your inventory slots (see below), and a complete list of your carried items. Selecting an inventory slot (with enter or a letter key), either puts away an existing item from the slot, or opens a list of items that you can equip in the slot.

Items are equipped in different inventory slots:\vskip 2pt
\begin{itemize}
	\item Wielded - your currently used melee weapon or firearm
	\item Prepared - the weapon to switch to when pressing ``z''
	\item Body - item worn on your body (e.g. an armor or a jacket)
	\item Head - item worn on your head (e.g. a gas mask)
\end{itemize}

\vskip 3pt

(2) Pressing ``a'' shows only items which you can ``activate'' or consume.\newline
This is merely a convenience feature - you can just as well use items by selecting them from the ``i'' screen, but with this method you get a nice filtered list\linebreak so you can see at a glance which consumables you have.

(3) Pressing ``d'' opens a list to select an item to drop.

(4) Pressing ``t'' opens a list to select an item to throw.\newline
The item stack previously thrown from (if any) is shown at the top with\linebreak the ``t'' key assigned (for convenience). If there is no stack previously thrown from, the ``t'' key is not assigned to any item. Selecting an item in this menu will show an aiming marker on the map, to select where to throw the chosen item.

Note that explosives are not used like throwing weapons. Instead they are used by first igniting or priming the explosive, this is done by ``activating''\linebreak it (can be done from the ``a'' inventory screen, see above) - now you can see your character colored yellow as a warning that you are holding an ignited explosive. Press ``t'', and aim somewhere (in some cases the game will mark\linebreak the area around your aim position to indicate the explosion size - be careful though, there may be unseen objects blocking the throw path). Press ``t'' again to throw the explosive toward the aim position.

In any inventory screen, you can see percentage values to the far right\linebreak of each item. This is how many percent of your total carried weight that each item takes (the numbers roughly adds up to 100\%, but there will be rounding errors, since decimals are not shown). So if you are overburdened, an item with a high percentage value might be a good candidate to drop.

\newpage

\section*{Hit Points, Wounds and healing}
\label{sec:hitpoints}

The state of your physical health is described in two ways:

(1) Hit Points (HP)\newline
This represents minor wounds like bruises and sprains, and also general combat morale, fatigue, stance, etc. If you HP reaches 0, it means you received\linebreak a killing blow, or the circumstances are such that you can no longer fight back (you are fallen, pinned or completely exhausted). Hit Points usually regenerate automatically over time.

(2) Wounds\newline
This represents more serious long-term damage. Wounds are received when you take a high amount of damage in one hit. Each Wound reduces your fighting abilities, maximum HP, and HP regeneration rate. Five concurrent wounds results in death.

There is an item called a ``Medical Bag'', which has a certain number of ``supplies''. When activated, it will either sanitize an infection or treat a Wound. When bitten or clawed by certain monsters, you can become Infected; this should be treated before the infection turns into disease! Treating infections only requires a few supplies, while treating wounds require more.

\section*{Spirit and spell casting}
\label{sec:spirit}

In addiction to your physical and mental health, you have an ``essence'' - or Spirit - to preserve. If you lose all your Spirit, you are dead.

Your Spirit Points are shown like this:\newline
\hspace*{8ex} Spirit~~~Current/Max

You can learn spells by casting them from manuscripts. Casting spells ``naturally'' (without manuscripts) will drain some of your spirit.

To cast memorized spell, press ``x'' to open a menu for selecting which spell\linebreak to cast. In this menu you can see the Spirit cost per spell (the cost is a range of values, and the spirit drained is a random value in this range), and your skill level for each spell (Occultist characters can learn to cast spells at higher levels by picking certain traits).

When casting a spell, you will get a warning if the spell could potentially drain all your remaining Spirit. You can then press y/n to attempt casting\linebreak or abort. Casting the spell in this situation is a gamble with death.

There are also other ways you can lose Spirit, for example some monsters can drain your spirit with their attacks.

\newpage

\section*{Insanity}
\label{sec:insanity}

``A few steps more, and our breaths were literally snatched from us by what we saw; so literally that Thornton, the psychic investigator, actually fainted in the arms of the dazed man who stood behind him. Norrys, his plump face utterly white and flabby, simply cried out inarticulately; whilst I think that what\linebreak I did was to gasp or hiss, and cover my eyes. The man behind me - the only one\linebreak of the party older than I - croaked the hackneyed `My God!' in the most cracked voice I ever heard. Of seven cultivated men, only Sir William Brinton retained his composure; a thing the more to his credit because he led the party and must have seen the sight first.'' - H.P. Lovecraft, ``Rats in the Walls''
\vskip 10pt

Certain events in this game affects the sanity and composure of your character. One of the main threats to your mental health are the various horrifying creatures that inhabit the game world. When inside your view, some creatures may cause a vague unease, while others are very disturbing to behold. Your mind also takes a hit when you use occult powers, carry weird artifacts,\linebreak or stand in the darkness, etc. Finally, there is a slow perpetual taxing of your mind from exploring and dwelling in this haunted dungeon.

Your mental health is represented by two percentage values:\vskip 2pt
\begin{itemize}
    \item ``Shock'' is your level of stress and paranoia in the current situation
    \item ``Insanity'' is long term, permanent madness
\end{itemize}

When various disturbing events occur, the shock value rises. When shock hits 100\%, the following happens:\vskip 2pt
\begin{itemize}
	\item You react somehow (scream, laugh, faint, babble, gain a phobia...)
	\item The shock value is restored
	\item Insanity rises
\end{itemize}

Each time you travel to the next dungeon level, you shock is restored (you have escaped the horrors above, and is bestowed with a sense of progress). Also, standing in a lit area reduces your shock a bit (most characters start with an Electric Lantern, which can be useful to prevent shock from rising too high).

If your Insanity reaches 100\%, you are hopelessly lost, and the game is over.

\section*{Sneaking and backstabbing}
\label{sec:sneaking}

Monsters unaware of your presence have a blue background on their map icon. This is their default mode. While in this state they may roam the map, but they will not actively search for you or purposefully attack you. If you attack an unaware opponent in melee, the attack does +50\% extra damage, and has\linebreak a very high chance to hit.

For each turn you spend in a monsters visual area (for monsters with sight -- some monsters are naturally blind), there is a chance that it will either completely detect you, or become ``suspicious''. While suspicious, the monsters has an increased chance of detecting you (watch out for messages about monsters looking ``wary'' or ``disturbed'').

Some monsters have the ability to sneak as well. You automatically attempt to detect them while they are in your visual area.

The chances of one creature detecting another are affected by:
\begin{itemize}
	\item Search skill
	\item The hidden actor's sneak skill
	\item Light and darkness
	\item Distance
\end{itemize}

When you stay out of an aware monster's vision for a certain number\linebreak of turns (and nothing else alerts it), it will ``forget'' about you. The number of turns this takes depends on the monster; a simple animal may only care for a couple of turns after it loses sight, while a human will remain aware longer.

Monsters also react to noise, and may disturb nearby monsters when becoming aware, alerting them to your presence. Medium and heavy melee weapons will make noise on attack, so a light weapon might be more ideal for a stealthy combat approach. Some sounds are you louder than others, for example firing a gun or kicking down a door is likely to attract some attention.

\section*{High scores and memorial files}
\label{sec:highscores}

Winning is not necessarily the sole purpose of Infra Arcana - there is also challenge to be found in exploring further, gaining more abilities, discovering more items, or encountering stranger and more powerful monsters than in previous attempts.

After each game is finished, information such as player name and dungeon level reached is stored in a ``memorial file'' on the disk, and a high score entry is added (unless you manually quit the game by pressing ``Q''). The high score list can be viewed by selecting the main menu option ``Graveyard''. Selecting a high score entry on this screen will show the corresponding stored\linebreak memorial file.

In addition to this, the top high score entries show up as gravestones\linebreak on the first level of the game (in the forest). Bumping a gravestone shows a short summary of the high score entry.

Score values are calculated based on your experience point (including points gained after the maximum character level is reached), which is the multiplied by several factors.

The factors contributing to a higher score value are:
\begin{itemize}
	\item Experience points gained
	\item Dungeon level reached
	\item Fewer turns spent
	\item Lower insanity
\end{itemize}

Also, when winning the game, some large bonus factors are added. Every winning game is guaranteed to have a better score than any non-winning game.

\newpage

\end{document}
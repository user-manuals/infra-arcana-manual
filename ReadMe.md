### About this repository.

This repository contains:

- pregenerated game manual in the PDF format,
- source code in the tex format,
- all necessary graphics,
- original manual.txt, copied from the latest stable game release.

Please note that **this is not an official work.** If you are looking for official Infra Arcana source code repository, you can find it here: https://gitlab.com/martin-tornqvist/ia. For official game website, visit https://sites.google.com/site/infraarcana/home.

I aim to keep this LaTeX-ed manual in sync with game's releases. The text of the instructions will not be changed, except in special circumstances (eg "modifying this document will change the text in the game" would become "modifying the manual.txt file will change the text in the game").